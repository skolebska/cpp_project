#include "gamewindow.h"

GameWindow::GameWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameWindow)

{
    ui->setupUi(this);
    connect(ui->workButton, SIGNAL(clicked()), this, SLOT(work()));
    connect(ui->attackButton, SIGNAL(clicked()), this, SLOT(attack()));
    connect(ui->recruitButton, SIGNAL(clicked()), this, SLOT(recruit()));
    connect(ui->cropsButton, SIGNAL(clicked()), this, SLOT(crops()));
    connect(ui->quitButton, SIGNAL(clicked()), this, SLOT(finish()));

    ui->graphicsView->setStyleSheet("border-image: url(/home/sylwia/sylwia/build-GOT_app-Desktop-Debug/map.png) 0 0 0 0 stretch stretch");
}

void GameWindow::setParams()
{
    std::vector<CKingdom> kingdoms = sim->getWorld().getKingdoms();
    ui->nameLabel->setText(sim->player->getName());

    QList<QLabel *> list = this->findChildren<QLabel *>();
    foreach(QLabel *l, list)
    {
        for (std::vector<CKingdom>::iterator it = kingdoms.begin(); it < kingdoms.end(); it++)
        {
            if (l->objectName() == sim->getStringKingdom())
            {
                l->setStyleSheet("QLabel { background-color : red;}");
            }
            if (l->objectName() == it->getName())
            {
                l->setText(QString::number(it->getPoints()));
            }
        }
    }
}

void GameWindow::setOwnValues()
{
     long choice = static_cast< long>(sim->player->getKingdom());
    ui->influenceLabel->setText(QString::number(sim->getWorld().getKingdoms().at(choice).getAtrrList().at(0)->getPoints() +
                                                sim->getWorld().getKingdoms().at(choice).getAtrrList().at(1)->getPoints()));
    ui->armyLabel->setText(QString::number(sim->getWorld().getKingdoms().at(choice).getPeopleList().at(0)->getPoints() +
                                           sim->getWorld().getKingdoms().at(choice).getPeopleList().at(1)->getPoints()));
    ui->inhabitantsLabel->setText(QString::number(sim->getWorld().getKingdoms().at(choice).getPeopleList().at(2)->getPoints() +
                                                  sim->getWorld().getKingdoms().at(choice).getPeopleList().at(3)->getPoints() +
                                                  sim->getWorld().getKingdoms().at(choice).getPeopleList().at(4)->getPoints()));
    ui->resourcesLabel->setText(QString::number(sim->getWorld().getKingdoms().at(choice).getAtrrList().at(2)->getPoints() +
                                                 sim->getWorld().getKingdoms().at(choice).getAtrrList().at(3)->getPoints() +
                                                 sim->getWorld().getKingdoms().at(choice).getAtrrList().at(4)->getPoints()));
    ui->sumLabel->setText(QString::number(sim->getWorld().getKingdoms().at(choice).getPoints()));
}

void GameWindow::work()
{
    sim->work();
    sim->simulate();
    setParams();
    setOwnValues();
}

void GameWindow::attack()
{
    sim->attack(this);
    sim->simulate();
    setParams();
    setOwnValues();
}

void GameWindow::recruit()
{
    sim->recruit();
    sim->simulate();
    setParams();
    setOwnValues();
}

void GameWindow::crops()
{
    sim->crops();
    sim->simulate();
    setParams();
    setOwnValues();
}

void GameWindow::finish()
{
    this->close();
}

GameWindow::~GameWindow()
{
    delete ui;
}
