#ifndef CINHABITANTS_H
#define CINHABITANTS_H
#include <QString>
#include <QPointF>
#include <CPeople.h>

class CInhabitants : public CPeople
{
public:
    CInhabitants(QString n,
        int p,
        int a,
        int h,
        int hap);
    ~CInhabitants();
    void refresh();
    void move();
    void work();
    virtual void changePoints(int how_much);
    virtual int getPoints();
protected:
private:
    QPointF position;
    QString name;
    int points;
    int amount;
    int hp;
    int happines;

};

#endif // CINHABITANTS_H
