#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <CInfluence.h>

CInfluence::CInfluence(QString n,
                       int p):
                    name(n), points(p)
{
}

int CInfluence::getPoints()
{
    return points;
}

void CInfluence::changePoints(int how_much)
{
    if (how_much < 0 && points < std::abs(how_much))
        points = 0;
    else
        points += how_much;
}

QString CInfluence::getName()
{
    return name;
}

CInfluence::~CInfluence()
{
    std::cout << "Influence destctor()\n";
}
