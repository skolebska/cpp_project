#ifndef CARMY_H
#define CARMY_H
#include <QString>
#include <QPointF>
#include <CPeople.h>

class CArmy : public CPeople
{
public:
    CArmy(QString n = "none",
            int p = 0,
            int a = 0,
            int h = 0,
            int hap = 0);
     ~CArmy();
     void refresh();
     void move();
     void attack();
     virtual void changePoints(int how_much);
     virtual int getPoints();

protected:
private:
    QPointF position;
    QString name;
    int points;
    int amount;
    int hp;
    int happines;

};

#endif // CARMY_H
