#ifndef COBJECT_H
#define COBJECT_H
#include <QString>
#include <QPointF>

class CObject
{
public:
    CObject() {}
    ~CObject() {}
     void refresh();

protected:
private:
    QPointF position;
    QString name;
    int points;
};

#endif // COBJECT_H
