#ifndef CATRRIBUTE_H
#define CATRRIBUTE_H
#include <QString>
#include <CObject.h>

class CAtrribute : public CObject
{
public:
    CAtrribute() {}
    virtual ~CAtrribute() = 0;
    void refresh();
    virtual void changePoints(int how_much) = 0;
    virtual int getPoints() = 0;

protected:
private:
    QString name;
    int points;
};

#endif // CATRRIBUTE_H
