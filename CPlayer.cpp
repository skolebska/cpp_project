#include <CPlayer.h>

CPlayer::CPlayer(int k,
                 QString n,
                 int p) :
          kingdom(k), name(n), points(p)
{

}

void CPlayer::setKingdom(int k)
{
    this->kingdom = k;
}

void CPlayer::setName(QString n)
{
    this->name = n;
}

int CPlayer::getKingdom()
{
    return kingdom;
}

QString CPlayer::getName ()
{
    return this->name;
}

