#ifndef CPEOPLE_H
#define CPEOPLE_H
#include <QString>
#include <QPointF>
#include <CObject.h>

class CPeople : public CObject
{
public:
    CPeople() {}
    virtual ~CPeople() = 0;
    void refresh();
    void move();
    virtual void changePoints(int how_much) = 0;
    virtual int getPoints() = 0;

protected:
private:
    QPointF position;
    QString name;
    int points;
    int amount;
    int hp;
    int happines;

};

#endif // CPEOPLE_H
