#include <CSimulation.h>

CSimulation::CSimulation(CWorld* w,
                         CPlayer* p,
                         QTime t) :
          world(w), player(p), time(t)
{

}

CWorld CSimulation::getWorld()
{
    return *world;
}

QString CSimulation::getStringKingdom ()
{
    switch (player->getKingdom())
    {
        case 0:
            return "Stark";
        case 1:
            return "Lannister";
        case 2:
            return "Durrandon";
        case 3:
            return "Gardener";
        case 4:
            return "Martell";
        case 5:
            return "Arryn";
        case 6:
            return "Targaryen";
    }
}

void CSimulation::simulate()
{
    this->findWinner();
    this->unexpectedSituation();
}
void CSimulation::work()
{
    // change Resources
    for (int i = 2; i < 5; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getAtrrList().at(i)->changePoints(20);
    }
    // change Army
    for (int i = 0; i < 2; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getPeopleList().at(i)->changePoints(-5);
    }
    // change Inhabitants
    for (int i = 2; i < 5; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getPeopleList().at(i)->changePoints(10);
    }
}
void CSimulation::attack(QWidget* par)
{
    int attackedKingdom = 6;
    bool ok;
    attackedKingdom = QInputDialog::getInt(par, QObject::tr("QInputDialog::getInteger()"),
                                 QObject::tr("Kingdom:"), 1, 1, 7, 1) - 1;

    QMessageBox msgBox;
    msgBox.setText("Attack!!! Kingdom number: " +  QString::number(attackedKingdom + 1) );
    msgBox.exec();

    // change Resources
    for (int i = 2; i < 5; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getAtrrList().at(i)->changePoints(-(rand() % 30 + 20));
    }
    // change Army
    for (int i = 0; i < 2; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getPeopleList().at(i)->changePoints(-(rand() % 30 + 20));
    }
    // change Influence
    for (int i = 0; i < 2; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getAtrrList().at(i)->changePoints(rand() % 30 + 20);
    }

    // ATTACKED KINGDOM
    // change Resources
    for (int i = 2; i < 5; i++)
    {
        this->getWorld().getKingdoms().at(attackedKingdom).getAtrrList().at(i)->changePoints(-(rand() % 30 + 20));
    }
    // change Army
    for (int i = 0; i < 2; i++)
    {
        this->getWorld().getKingdoms().at(attackedKingdom).getPeopleList().at(i)->changePoints(-(rand() % 30 + 20));
    }
    // change Influence
    for (int i = 0; i < 2; i++)
    {
        this->getWorld().getKingdoms().at(attackedKingdom).getAtrrList().at(i)->changePoints(rand() % 30 + 20);
    }
}
void CSimulation::recruit()
{
    // change Army
    for (int i = 0; i < 2; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getPeopleList().at(i)->changePoints(40);
    }
    // change Inhabitants
    for (int i = 2; i < 5; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getPeopleList().at(i)->changePoints(-20);
    }
}
void CSimulation::crops()
{
    // change Resources
    for (int i = 2; i < 5; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getAtrrList().at(i)->changePoints(-(rand() % 30 + 30));
    }
    // change Influence
    for (int i = 0; i < 2; i++)
    {
        this->getWorld().getKingdoms().at(player->getKingdom()).getAtrrList().at(i)->changePoints(rand() % 10 + 10);
    }
}

void CSimulation::unexpectedSituation()
{
    int whichKingdom = rand() % 7;
    int whichSituation = rand() % 5;

    QMessageBox msgBox;
    msgBox.setText("Unexpected situation is happening! Situation number: " + QString::number(whichSituation) +
    " to Kingdom number: " +  QString::number(whichKingdom + 1) );
    msgBox.exec();

    switch(whichSituation) {
    // EPIDEMIC
    case 0:
        // change Army
        for (int i = 0; i < 2; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getPeopleList().at(i)->changePoints(-20);
        }
        // change Inhabitants
        for (int i = 2; i < 5; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getPeopleList().at(i)->changePoints(-20);
        }
        break;
    // WILD ATTACK
    case 1:
        // change Army
        for (int i = 0; i < 2; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getPeopleList().at(i)->changePoints(-10);
        }
        // change Inhabitants
        for (int i = 2; i < 5; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getPeopleList().at(i)->changePoints(-10);
        }
        // change Resources
        for (int i = 2; i < 5; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getAtrrList().at(i)->changePoints(-10);
        }
        // change Influence
        for (int i = 0; i < 2; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getAtrrList().at(i)->changePoints(-10);
        }
        break;
    // BAD CROPS
    case 2:
        // change Resources
        for (int i = 2; i < 5; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getAtrrList().at(i)->changePoints(-10);
        }
        // change Influence
        for (int i = 0; i < 2; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getAtrrList().at(i)->changePoints(-10);
        }
        break;
    // GOOD CROPS
    case 3:
        // change Resources
        for (int i = 2; i < 5; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getAtrrList().at(i)->changePoints(10);
        }
        // change Influence
        for (int i = 0; i < 2; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getAtrrList().at(i)->changePoints(10);
        }
        break;
    // IMMIGRANTS
    case 4:
        // change Inhabitants
        for (int i = 2; i < 5; i++)
        {
            this->getWorld().getKingdoms().at(whichKingdom).getPeopleList().at(i)->changePoints(30);
        }
        break;

    }





}

void CSimulation::findWinner()
{
    int max_points = 0;
    int other_player_points = 0;
    for (int i = 0; i < 7; i++)
    {
        if (this->getWorld().getKingdoms().at(static_cast< long>(i)).getPoints() > max_points)
        {
            max_points = this->getWorld().getKingdoms().at(static_cast< long>(i)).getPoints();
            this->winner = this->getWorld().getKingdoms().at(static_cast< long>(i)).getName();
        }
    }
    for (int i = 0; i < 7; i++)
    {
        if (this->getWorld().getKingdoms().at(static_cast< long>(i)).getName() != this->winner)
        {
            other_player_points += this->getWorld().getKingdoms().at(static_cast< long>(i)).getPoints();
        }
    }
    if (other_player_points*2 < max_points)
    {
        QMessageBox msgBox;
        msgBox.setText("The winner is:" + this->winner + "!!!");
        msgBox.exec();

        //FINISH EVERYTHING
    }
}
