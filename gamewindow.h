#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H
//#include <QMainWindow>
#include <CWorld.h>
#include <QString>
#include <QDialog>
#include <CSimulation.h>
#include "ui_gamewindow.h"
#include <iostream>
#include <CAtrribute.h>


namespace Ui {
    class GameWindow;
}

class GameWindow : public QDialog
{
    Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = 0);
    ~GameWindow();
    void setParams();
    Ui::GameWindow *ui;
    CSimulation* sim;
public slots:
    void setOwnValues();
    void work();
    void attack();
    void recruit();
    void crops();
    void finish();

private:

};


#endif // GAMEWINDOW_H
