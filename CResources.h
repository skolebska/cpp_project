#ifndef CRESOURCES_H
#define CRESOURCES_H
#include <QString>
#include <CAtrribute.h>


class CResources : public CAtrribute
{
public:
    CResources(QString n,
               int p);
    ~CResources();
    void refresh();
    virtual void changePoints(int how_much);
    virtual  int getPoints();

protected:
private:
    QString name;
     int points;

};



#endif // CRESOURCES_H
