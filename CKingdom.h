#ifndef CKINGDOM_H
#define CKINGDOM_H
#include <QPointF>
#include <QString>
#include <vector>
#include "CPeople.h"
#include "CAtrribute.h"

class CKingdom
{
    public:
        CKingdom(QPointF pos,
                 QString n,
                 float h,
                 float w,
                 int p);
        ~CKingdom();
        QPointF getPosition();
        QString getName();
        int getPoints();
        std::vector<CAtrribute*> getAtrrList();
        std::vector<CPeople*> getPeopleList();
        void setPeopleList(std::vector<CPeople*> list);
        void setAtrrList(std::vector<CAtrribute*> list);
        void setPeopleList();
    protected:
    private:
        QPointF position;
        QString name;
        float height;
        float width;
        int points;
        std::vector<CAtrribute*> AtrrList;
        std::vector<CPeople*> PeopleList;
};

#endif // CKINGDOM_H
