#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include "CInhabitants.h"

CInhabitants::CInhabitants(QString n,
               int p,
               int a,
               int h,
               int hap):
        name(n), points(p), amount(a), hp(h), happines(hap)
{

}

void CInhabitants::changePoints(int how_much)
{
    if (how_much < 0 && points < std::abs(how_much))
        points = 0;
    else
        points += how_much;
}

int CInhabitants::getPoints()
{
    return points;
}

CInhabitants::~CInhabitants()
{
    std::cout << "Influence destctor()\n";
}
