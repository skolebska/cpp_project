#ifndef CPLAYER_H
#define CPLAYER_H
#include <QString>
#include <iostream>

class CPlayer
{
public:
    CPlayer(int k,
            QString n,
            int p);
     ~CPlayer();
    void setKingdom(int k);
    void setName(QString n);
    int getKingdom();
    QString getName();
protected:
private:
    int kingdom;
    QString name;
    int points;
};

#endif // CPLAYER_H
