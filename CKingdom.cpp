#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <CKingdom.h>

CKingdom::CKingdom(QPointF pos,
                   QString n,
                   float h,
                   float w,
                   int p):
              position(pos), name(n), height(h), width(w), points(p)
{

}

CKingdom::~CKingdom() {}

QPointF CKingdom::getPosition()
{
    return position;
}

QString CKingdom::getName()
{
    return name;
}

int CKingdom::getPoints()
{
    for (int i = 0; i < 5; i++)
    {
        points += this->AtrrList.at(static_cast< long>(i))->getPoints();
        points += this->PeopleList.at(static_cast< long>(i))->getPoints();
    }
    return points;
}

void CKingdom::setAtrrList(std::vector<CAtrribute*> list)
{
    this->AtrrList = list;
}

void CKingdom::setPeopleList(std::vector<CPeople*> list)
{
    this->PeopleList = list;
}

std::vector<CAtrribute*> CKingdom::getAtrrList()
{
    return AtrrList;
}

std::vector<CPeople*> CKingdom::getPeopleList()
{
    return PeopleList;
}
