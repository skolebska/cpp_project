#ifndef CWORLD_H
#define CWORLD_H
#include <vector>
#include <CKingdom.h>
#include <CInfluence.h>
#include <CResources.h>
#include <stdlib.h>
#include <iostream>
#include <CResources.h>
#include <CPeople.h>
#include <CArmy.h>
#include <CInhabitants.h>

class CWorld
{
public:
    CWorld(int h,
       int w);
    ~CWorld() {}
    std::vector<CKingdom> getKingdoms();
    std::vector<CAtrribute*> chooseAtrrList();
    std::vector<CPeople*> choosePeopleList();
    void generate();
    void refresh();

protected:
private:
    int height;
    int width;
    std::vector<CKingdom> kingdomGraph;
};


#endif // CWORLD_H
