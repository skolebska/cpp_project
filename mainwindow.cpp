#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lineEdit->setText("Insert the kingdom name");
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(openNewWindow()));
    newGameWindow = new GameWindow();
}
void MainWindow::CreateLines(std::vector<CKingdom> list)
{
    for (std::vector<CKingdom>::iterator it = list.begin(); it < list.end(); it++)
    {
        ui->comboBox->addItem(it->getName());
    }
}

void MainWindow::openNewWindow()
{
    newGameWindow->sim->player->setKingdom(getKingdomChoice());
    newGameWindow->sim->player->setName(getNameChoice());
    newGameWindow->setParams();
    newGameWindow->setOwnValues();
    newGameWindow->show();
    this->hide();
}

int MainWindow::getKingdomChoice()
{
    return ui->comboBox->currentIndex();
}

QString MainWindow::getNameChoice()
{
    return ui->lineEdit->text();
}

void MainWindow::setSimulation(CSimulation *sim)
{
   newGameWindow->sim = sim;
}

MainWindow::~MainWindow()
{
    delete ui;
}
