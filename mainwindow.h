#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QComboBox>
#include <CKingdom.h>
#include <gamewindow.h>
#include <CSimulation.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void CreateLines(std::vector<CKingdom> list);
    int getKingdomChoice();
    QString getNameChoice();
    void setSimulation(CSimulation *sim);
    ~MainWindow();
    GameWindow* newGameWindow;

public slots:
    void openNewWindow();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
