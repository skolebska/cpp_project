#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <vector>
#include <CWorld.h>
#include <QPointF>
#include <CSimulation.h>
#include "gamewindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QTime time;

    CWorld* world = new CWorld (500, 500);
    CPlayer* player = new CPlayer(0, "", 0);
    CSimulation* simulation = new CSimulation(world, player, time);

    w.show();
    w.CreateLines(world->getKingdoms());
    w.setSimulation(simulation);

    // \/ 6. działanie po wybraniu przycisków
    //       \/   work
    //       \/   attack
    //        \/  xxx
    //        \/  crops
    // 7. dodac wyswietlanie czasu oraz zwiększanie tego czasu
    // \/ 8. dodac zdarzenia losowe
    // \/ 9. wrzucic na bitbucket




    return a.exec();
}
