#ifndef CINFLUENCE_H
#define CINFLUENCE_H
#include <QString>
#include <CAtrribute.h>

class CInfluence : public CAtrribute
{
public:
    CInfluence(QString n = "none",
               int p = 0);
    ~CInfluence();
    void refresh();
    virtual void changePoints(int how_much);
    virtual  int getPoints();
    QString getName();
protected:
private:
    QString name;
     int points;
};


#endif // CINFLUENCE_H
