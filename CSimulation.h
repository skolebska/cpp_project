#ifndef CSIMULATION_H
#define CSIMULATION_H

#include <QString>
#include <CWorld.h>
#include <CPlayer.h>
#include <QTime>
#include <QMessageBox>
#include <QInputDialog>
#include <QTranslator>
#include <QWidget>

class CSimulation
{
public:
    CSimulation(CWorld* w,
                CPlayer* p,
                QTime t);
    ~CSimulation();
    CWorld* world;
    CPlayer* player;
    QTime time;
    QString getParams();
    CWorld getWorld();
    QString getStringKingdom();
    void simulate();
    void findWinner();
    void work();
    void attack(QWidget* par);
    void crops();
    void recruit();
    void unexpectedSituation();
protected:

private:
    QString winner;
};


#endif // CSIMULATION_H
