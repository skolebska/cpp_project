#include <CWorld.h>

CWorld::CWorld(int h,
               int w) :
          height(h), width(w)
{
    this->generate();
}

std::vector<CKingdom> CWorld::getKingdoms()
{
    return kingdomGraph;
}

void CWorld::generate()
{
    QPointF position1 (0,0);
    QPointF position2 (10,10);
    std::vector<QString> kingdomNames;
    kingdomNames.push_back("Stark");
    kingdomNames.push_back("Lannister");
    kingdomNames.push_back("Durrandon");
    kingdomNames.push_back("Gardener");
    kingdomNames.push_back("Martell");
    kingdomNames.push_back("Arryn");
    kingdomNames.push_back("Targaryen");

    for (int i = 0; i < 7; i++)
    {
        kingdomGraph.push_back(CKingdom(position1, kingdomNames.at(static_cast< long>(i)), rand() % 10 + 10, rand() % 10 + 10, rand() % 10 + 130));
        kingdomGraph.at(static_cast< long>(i)).setAtrrList(CWorld::chooseAtrrList());
        kingdomGraph.at(static_cast< long>(i)).setPeopleList(CWorld::choosePeopleList());
    }
}

std::vector<CAtrribute*> CWorld::chooseAtrrList()
{
    std::vector<CAtrribute*> list;

    CAtrribute* p1 = new CInfluence("influenceWest", rand() % 10 + 40);
    list.push_back(p1);
    CAtrribute* p2 = new CInfluence("influenceWest", rand() % 10 + 40);
    list.push_back(p2);
    CAtrribute* p3 = new CResources("resourcesGold", rand() % 10 + 40);
    list.push_back(p3);
    CAtrribute* p4 = new CResources("resourcesFood", rand() % 10 + 40);
    list.push_back(p4);
    CAtrribute* p5 = new CResources("resourcesWood", rand() % 10 + 40);
    list.push_back(p5);

    return list;
}

std::vector<CPeople*> CWorld::choosePeopleList()
{
    std::vector<CPeople*> list;

    CPeople* p1 = new CArmy("armyLand", rand() % 10 + 40, rand() % 10 + 40,
                                        rand() % 10 + 40, rand() % 10 + 40);
    list.push_back(p1);
    CPeople* p2 = new CArmy("armyRiders", rand() % 10 + 40, rand() % 10 + 40,
                                          rand() % 10 + 40, rand() % 10 + 40);
    list.push_back(p2);
    CPeople* p3 = new CInhabitants("man", rand() % 10 + 40, rand() % 10 + 40,
                                          rand() % 10 + 40, rand() % 10 + 40);
    list.push_back(p3);
    CPeople* p4 = new CInhabitants("woman", rand() % 10 + 40, rand() % 10 + 40,
                                            rand() % 10 + 40, rand() % 10 + 40);
    list.push_back(p4);
    CPeople* p5 = new CInhabitants("children", rand() % 10 + 40, rand() % 10 + 40,
                                               rand() % 10 + 40, rand() % 10 + 40);
    list.push_back(p5);

    return list;
}

