#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <CResources.h>

CResources::CResources(QString n,
                       int p):
                name(n), points(p)
{

}

int CResources::getPoints()
{
    return points;
}

void CResources::changePoints(int how_much)
{
    if (how_much < 0 && points < std::abs(how_much))
        points = 0;
    else
        points += how_much;
}

CResources::~CResources()
{

}
